<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "myDB";

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    //prepare and bind
    $stmt = $conn->prepare("INSERT INTO MyGuests(firstname,lastname,email)VALUES(?,?,?)");
    $stmt->bind_param("sss", $firstname, $lastname, $email);

    //set parameters and execute
    $firstname = "Geet";
    $lastname = "sharma";
    $email = "geeta@gmail.com";
    $stmt->execute();

    $firstname = "Seeta";
    $lastname = "sharma";
    $email = "seeta@gmail.com";
    $stmt->execute();

    $firstname = "Reeta";
    $lastname = "sharma";
    $email = "reeta@gmail.com";
    $stmt->execute();

    echo "New records created successfully";

    $stmt->close();
    $conn->close();
    ?>
</body>

</html>