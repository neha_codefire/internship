<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "myDB";

    //Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    //check connection
    if ($conn->connect_error) {
        die("Connection Failed:" . $conn->connect_error);
    }
    $sql = "INSERT INTO MyGuests (firstname,lastname,email)VALUES('Neha','Maurya','neha@example.com')";
    if ($conn->query($sql) === TRUE) {
        //to retrieve the ID of the last inserted/updated record
        $last_id = mysqli_insert_id($conn);
        echo "Last inserted ID is:" . $last_id;
        echo "New record created successfully";
    } else {
        echo "Error:" . $sql . "<br>" . $conn->error;
    }
    $conn->close();
    ?>
</body>

</html>