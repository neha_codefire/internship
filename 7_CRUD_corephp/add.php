<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    include 'include/head.php';
    include 'include/connection.php';
    ?>
    <link rel="stylesheet" href="assets/css/page.css">

</head>

<body>
    <div class="container reg-form">
        <?php include 'include/header.php';  ?>
        <h2>Add User</h2>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" onsubmit="" name="myform" method="post">
            <div class="form-group">
                <label for="first-name">First Name(required)<large class="text-danger">*</large></label>
                <input type="text" class="form-control" name="fname" id="first-name">
                <span class="Error" id="fnameError"></span>
            </div>
            <div class="form-group">
                <label for="last-name">Last Name(required)<large class="text-danger">*</large></label>
                <input type="text" class="form-control" name="lname" id="last-name"><span class="Error" id="lnameError"></span>
            </div>
            <div class="form-group">
                <label for="email">Email(required)<large class="text-danger">*</large></label>
                <input type="email" class="form-control" name="email" id="email" placeholder="Enter email"><span class="Error" id="emailError"></span>
            </div>
            <div class="form-group">
                <label for="pwd">Password(required)<large class="text-danger">*</large></label>
                <input type="password" class="form-control" id="pwd" name="pwd" placeholder="Enter password">
                <span class="Error" id="pwdError"></span>
            </div>
            <div class="form-group">
                <label for="cpwd">Confirm Password(required)<large class="text-danger">*</large></label>
                <input type="password" class="form-control" id="cpwd" name="cpwd" placeholder="Enter password">
                <span class="Error" id="cpwdError"></span>
            </div>
            <button type="submit" class="btn btn-primary" id="submitbtn">Submit</button>
            <a href="#" class="btn btn-primary float-right px-3" onclick="document.location='index.php'"> Back</a>
        </form>
    </div>
    <?php
    $fname = $lname = $email = $pwd = $cpwd = "";

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $email = test_input($_POST["email"]);
        $sql = "SELECT email FROM users WHERE email='$email'";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            echo "<script> alert('User Already Exists')</script>";
        } else {


            $stmt = $conn->prepare("INSERT INTO users(first_name,last_name,email,pwd,cpwd)VALUES(?,?,?,?,?)");
            $stmt->bind_param("sssss", $fname, $lname, $email, $pwd, $cpwd);

            $fname = test_input($_POST["fname"]);
            $lname = test_input($_POST["lname"]);
            //$email = test_input($_POST["email"]);
            $pwd = test_input($_POST["pwd"]);
            $cpwd = test_input($_POST["cpwd"]);
            $stmt->execute();

            echo "<script> alert('New user added successfully')</script>";

            $stmt->close();
            $conn->close();
        }
    }

    function test_input($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
    ?>

</body>
<script src="assets/js/jquery-3.6.0.min.js"></script>
<script type="text/javascript" src="assets/js/crud.js"></script>

</html>