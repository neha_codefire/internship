$(document).ready(function () {


    let fnameError = true;
    let lnameError = true;
    let emailError = true;
    let pwdError = true;
    let cpwdError = true;
    let fileError = true;
    let regxname = /^[a-zA-Z-' ]*$/;
    //first name
    function validateFirstName() {
        let fname = $('#first-name').val();

        if (fname.trim().length == '') {
            $('#fnameError').text("First Name is missing");
            fnameError = false;
            return false;
        }
        else if ((fname.trim().length < 3) || (fname.trim().length > 10)) {
            $("#fnameError").text("length of First must be between 3 and 10");
            fnameError = false;
            return false;
        }
        else if (!regxname.test(fname)) {
            $("#fnameError").text("Only letters and white space allowed");
            fnameError = false;
            return false;
        }
        else {
            $("#fnameError").text("");
        }
    }

    //last name
    function validateLastName() {
        let lname = $('#last-name').val();
        if (lname.trim().length == '') {
            $('#lnameError').text("Last Name is missing");
            lnameError = false;
            return false;
        }
        else if ((lname.trim().length < 3) || (lname.trim().length > 10)) {
            $("#lnameError").text("length of lastname must be between 3 and 10");
            fnameError = false;
            return false;
        }
        else if (!regxname.test(lname)) {
            $("#fnameError").text("Only letters and white space allowed");
            fnameError = false;
            return false;
        }
        else {
            $("#lnameError").text("");
        }
    }

    //email
    function validateEmail() {
        let email = $('#email').val();
        let regxEmail = /^([a-zA-Z0-9\.-]+)@([a-zA-Z0-9-]+).([a-z]{2,8})(.[a-z]{2,8})?$/;
        if (email.trim().length == '') {
            $('#emailError').text("Email is missing");
            emailError = false;
            return false;
        }
        else if (!regxEmail.test(email)) {
            $("#emailError").text("Invalid email address");
            emailError = false;
            return false;
        }
        else {
            $("#emailError").text("");
        }
    }

    //password
    function validatePwd() {
        let pwd = $('#pwd').val();
        let cpwd = $('#cpwd').val();
        let regxPass = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;
        if (pwd.trim().length == '') {
            $('#pwdError').text("Password is missing");
            emailError = false;
            return false;
        }
        else if (!regxPass.test(pwd)) {
            $("#pwdError").text("Min length 8 characters | must contain atleast one digit,one upper case alphbet,one lower case alphabet and atleast 1 character:!@#$%&*()-+=^ | it doesn't contain any white space");
            pwdError = false;
            return false;
        }
        else {
            $("#pwdError").text("");
        }
    }

    // //Confirm password
    function validateCpwd() {
        let cpwd = $('#cpwd').val();
        let pwd = $('#pwd').val();
        if (cpwd.trim().length == '') {
            $('#cpwdError').text("Enter password to confirm");
            cpwdError = false;
            return false;
        }
        else if (cpwd != pwd) {
            $('#cpwdError').text("Password doesn't match");
            cpwdError = false;
            return false;
        }
        else {
            $("#cpwdError").text("");
        }
    }
    function validateFile() {
        let file = $(":file").val();
        if (file == "") {
            $('#fileError').text("No photo Selected");
            fileError = false;
            return false;
        }
        else {
            $("#fileError").hide();
        }
    }

    //submit
    $('#submitbtn').click(function () {
        validateFirstName();
        validateLastName();
        validateEmail();
        validatePwd();
        validateCpwd();

        if ((fnameError == true && lnameError == true && emailError == true && pwdError == true && cpwdError == true)) {
            return true;
        }
        else {
            return false;
        }
    });
    $('#submit3').click(function () {
        validateFile();
        if (fileError == true) {
            return true;
        }
        else {
            return false;
        }
    });

    $('#submit1').click(function () {
        validateFirstName();
        if (fnameError == true) {
            return true;
        }
        else {
            return false;
        }
    });

    $('#submit2').click(function () {
        validateLastName();
        if (lnameError == true) {
            return true;
        }
        else {
            return false;
        }
    });

});