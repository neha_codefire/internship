<!DOCTYPE html>
<html lang="en">

<head>
    <?php
include 'include/head.php';
include 'include/connection.php';
$id = $_GET['id'];
?>
</head>

<body>
    <div class="container">
        <?php include 'include/header.php';?>
        <h2>Update User</h2>
        <!-- profile pic -->
        <form action="#" onsubmit="" name="myform" method="post" enctype="multipart/form-data">

        <?php
$sql = "SELECT photo FROM users WHERE id='$id'";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        if ($row['photo'] != null) {
            echo "<img width='225px' height='228px'     class='my-3' src='assets/images/" . $row['photo'] . "'>";
        } else {
            echo "<img class='my-3' src='assets/images/default.png'>";
        }
    }
}
?>
            <div class="form-group">
                <input type="file" class="form-control" name="image" id="image">
                <span style="color:red;" id="fileError"></span>
            </div>

            <button type="submit" name="submit3" class="btn btn-primary mb-3" id="submit3">UPDATE PROFILE PHOTO</button>
        </form>

        <!-- First Name -->
        <form action="#" onsubmit="" name="myform" method="post">
            <div class="form-group">
                <?php
$sql = "SELECT first_name FROM users WHERE id='$id'";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $fname = $row['first_name'];
        ?>
                        <input type="text" class="form-control" name="fname" id="first-name" placeholder="<?php echo $fname ?>">
                        <span style="color:red;" class="Error" id="fnameError"></span>
            </div>
    <?php
}
}
?>
            <button type="submit" name="submit1" class="btn btn-primary mb-3 " id="submit1">UPDATE First Name</button>
        </form>

        <!-- Last name -->
        <form action="#" onsubmit="" name="myform" method="post">
            <?php
$sql = "SELECT last_name FROM users WHERE id='$id'";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $lname = $row['last_name'];
        ?>
            <div class="form-group">
                <input type="text" class="form-control" name="lname" id="last-name" placeholder="<?php echo $lname ?>"><span class="Error" id="lnameError" style="color:red;"></span>
            <?php
}
}
?>
            </div>
            <button type="submit" name="submit2" class="btn btn-primary mb-3" id="submit2">UPDATE Last Name</button>
        </form>
        <a href="#" class="btn btn-primary float-right px-3" onclick="document.location='index.php'"> Back</a>
    </div>



<?php
$newfname = $newlname = "";
//profile pic
if (isset($_POST['submit3'])) {
    $target = "assets/images/" . basename($_FILES['image']['name']);
    $image = $_FILES['image']['name'];
    $sql = "UPDATE users SET photo='$image' WHERE id='$id'";
    if ($conn->query($sql) === true) {
        if (move_uploaded_file($_FILES['image']['tmp_name'], $target)) {
            echo "<script> alert('Profile photo updated successfully')</script>";
        } else {
            echo "not done";
        }
    }
}
//First name
if (isset($_POST['submit1'])) {
    $newfname = ($_POST['fname']);
    $sql = "UPDATE users SET first_name='$newfname' WHERE id='$id'";
    if ($conn->query($sql) === true) {
        echo "<script> alert('First Name updated successfully')</script>";
    } else {
        echo "Error updating the record: " . $conn->error;
    }
}
//Last Name
if (isset($_POST['submit2'])) {
    $newlname = ($_POST['lname']);
    $sql = "UPDATE users SET last_name='$newlname' WHERE id='$id'";
    if ($conn->query($sql) === true) {
        echo "<script> alert('Last Name updated successfully')</script>";
    } else {
        echo "Error updating the record: " . $conn->error;
    }
}

$conn->close();
?>
</body>
<script src="assets/js/jquery-3.6.0.min.js"></script>
<script type="text/javascript" src="assets/js/crud.js"></script>

</html>