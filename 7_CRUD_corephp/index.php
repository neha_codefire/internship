<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    include 'include/head.php';
    include 'include/connection.php';
    ?>

</head>

<body>
    <div class="container-fluid  mx-auto m-4">
        <?php include 'include/header.php'; ?>
        <button class="btn btn-success float-right m-4 p-2" onclick="document.location='add.php'">Add User</button>
        <div>
            <table class='table table-striped table-hover table-bordered'>
                <thead class='thead-dark'>
                    <tr>
                        <th>Name</th>
                        <th>Email ID</th>
                        <th>View</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <?php
                $result_per_page = 5;
                $no_of_results = 0;
                //$starting_limit_number = 0;
                $sql = "SELECT id,first_name,last_name,email,photo FROM users";
                $result = $conn->query($sql);
                $no_of_results = mysqli_num_rows($result);

                //determine total no. of pages
                $number_of_pages = ceil($no_of_results / $result_per_page);
                //to see which page no. visitor is currently on
                if (!isset($_GET['page'])) { //if page valiable is not set
                    $page = 1; //set it to 1
                } else {
                    $page = $_GET['page'];
                }
                //determine the sql limit starting no. for the result on the displaying page
                //page 1: 0-5
                //page 2: 5-10
                //page 3: 10-15`x
                //starting_limit_number= (page1-1*5)=0
                //                       (page2-1*5)=5
                //                       (page3-1*5)=10
                //                      =(page_number-1)*result_per_page
                $this_page_first_result = ($page - 1) * $result_per_page;
                //retrieve selected results from database and display them on page
                $sql2 = "SELECT id,first_name,last_name,email,photo FROM users ORDER BY id LIMIT " . $this_page_first_result . ',' . $result_per_page;
                $result2 = $conn->query($sql2);
                if ($result2->num_rows > 0) {
                    while ($row = $result2->fetch_assoc()) {
                        $id = $row["id"];
                ?>
                        <tr>
                            <td><?php echo $row["first_name"] . " " . $row["last_name"] ?></td>
                            <td><?php echo $row["email"] ?></td>
                            <td><button class="btn btn-primary" onclick="document.location='view.php?id=<?php echo $id ?>'">View</button></td>
                            <td><button class='btn btn-primary' onclick="document.location='edit.php?id=<?php echo $id ?>'">Edit</button></td>
                            <td><button class='btn btn-primary' onclick="document.location='delete.php?id=<?php echo $id ?>'">Delete</button></td>
                        </tr>
                <?php
                    }
                } else {
                    echo "0 results";
                }

                $conn->close();
                ?>
            </table>
        </div>
        <!-- display the link to the pages -->
        <nav aria-label="Page navigation example">
            <ul class="pagination justify-content-center pagination-sm m-0">
                <?php
                for ($page = 1; $page <= $number_of_pages; $page++) {
                    echo '<li class="page-item"><a class="page-link" href="index.php?page=' . $page . '">' . $page . '</a></li>';
                }
                ?>
            </ul>
        </nav>
    </div>
</body>

</html>