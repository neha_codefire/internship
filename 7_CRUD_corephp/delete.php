<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    include 'include/head.php';
    include 'include/connection.php';
    ?>
</head>

<body>
    <?php
    include 'include/header.php';
    $id = $_GET['id'];
    $sql = "DELETE FROM users WHERE id='$id'";
    if ($conn->query($sql) === TRUE) {
    ?>
        <div class="container d-flex justify-content-center">
            <div class="card" style="width:400px">
                <div class="card-body">User Deleted successfully</div>
                <a href="#" class="btn btn-primary float-right px-3" onclick="document.location='index.php'"> Back</a>
            </div>

        </div>

    <?php
    } else {
        echo "Error deleting the record: " . $conn->error;
    }
    $conn->close();
    ?>

</body>

</html>