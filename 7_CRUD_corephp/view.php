<!DOCTYPE html>
<html lang="en">

<head>
    <?php
include 'include/head.php';
include 'include/connection.php';
$id = $_GET['id'];
?>
</head>

<body>
    <div class="container text-center">
        <?php include 'include/header.php';?>
        <div class="card " style="width:300px;margin:auto">
            <!-- <img class="card-img-top" src="img_avatar1.png" alt="Card image" style="width:100%"> -->
            <?php
$sql = "SELECT photo FROM users WHERE id='$id'";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        if ($row['photo'] != null) {

            echo "<img class='card-img-top' src='assets/images/" . $row['photo'] . "' style='width:100%`;height:340px'>";
        } else {
            ?>
                        <img class="card-img-top" src="assets/images/default.png" alt="Card image" style="width:100%">
            <?php
}
    }
}
?>
            <div class="card-body">
                <?php

$sql = "SELECT id,first_name,last_name,email FROM users WHERE id='$id'";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        ?>
                        <h4 class="card-title"><?php echo $row['first_name'] . " " . $row['last_name'] ?></h4>
                        <p class="card-text"><?php echo $row['email'] ?></p>

                <?php
}
} else {
    echo "0 results";
}
$conn->close();
//end of sql code
?>

                <a href="#" class="btn btn-primary" onclick="document.location='index.php'">Back</a>
            </div>
        </div>
    </div>
</body>

</html>