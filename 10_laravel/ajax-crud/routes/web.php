<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CustomerController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::resource('customers','App\Http\Controllers\CustomerController');
Route::post('customers/update', 'App\Http\Controllers\CustomerController@update')->name('customers.update');
Route::delete('customers/destroy/{id}', 'App\Http\Controllers\CustomerController@destroy');