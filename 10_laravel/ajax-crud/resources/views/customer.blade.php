<!DOCTYPE html>
<html>

<head>
    <title>Laravel Ajax CRUD </title>
    <meta name="csrf-token" content="">
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

    {{-- <script type="text/javascript" src="{{ URL::asset('js/jquery.js') }}"></script> --}}

    <style>
        .error {
            color: red;
            font-weight: 700;
            display: block;
            padding: 6px 0;
            font-size: 14px;
        }

    </style>
</head>

<body>

    <div class="container">
        <h1>Laravel Ajax CRUD </h1>
        <br>
        <a class="btn btn-success" href="javascript:void(0)" id="createNewCustomer"> Create New User</a>
        <br>
        <table class="table table-bordered data-table">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th width="280px">Action</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

    <div class="modal fade" id="ajaxModel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modelHeading"></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger print-error-msg" style="display:none">
                        <ul></ul>
                    </div>
                    <form id="CustomerForm" name="CustomerForm" class="form-horizontal">
                        @csrf
                        <input type="hidden" name="Customer_id" id="Customer_id">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <strong>First Name:</strong>
                                <input type="text" class="form-control" id="first_name" name="first_name"
                                    placeholder="Enter First Name" value="" maxlength="50">
                                <span style="color:red">@error('first_name'){{ $message }}@enderror</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-12">
                                    <strong>Last Name:</strong>
                                    <input type="text" class="form-control" id="last_name" name="last_name"
                                        placeholder="Enter Last Name" value="" maxlength="50">
                                    <span class="fnameE" style="color:red"></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-12">
                                    <strong>Email:</strong>
                                    <input type="text" class="form-control" id="email" name="email"
                                        placeholder="Enter Email" value="" maxlength="50">
                                    <span style="color:red">@error('email'){{ $message }}@enderror</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <strong>Password:</strong>
                                        <input type="password" class="form-control" id="password" name="password"
                                            placeholder="Password" value="" maxlength="50">
                                        <span style="color:red">@error('password'){{ $message }}@enderror</span>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <div class="form-group">
                                            <strong>Confirm Password:</strong>
                                            <input type="password" name="password_confirmation" class="form-control"
                                                placeholder="Confirm Password">
                                        </div>
                                    </div>

                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Add User
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- update Model --}}
                <div class="modal fade" id="ajaxModel2" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="modelHeading2"></h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form id="CustomerForm2" name="CustomerForm2" class="form-horizontal" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="Customer_id2" id="Customer_id2">
                                    <div class="form-group">
                                        <label for="first_name2" class="col-sm-2 control-label">First Name</label>
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control" id="first_name2" name="first_name2"
                                                placeholder="Enter First Name" value="" maxlength="50">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="last_name2" class="col-sm-2 control-label">Last Name</label>
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control" id="last_name2" name="last_name2"
                                                placeholder="Enter Last Name" value="" maxlength="50">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="file" class="col-sm-2 control-label">Profile Photo</label>
                                        <span id="store_image"></span>
                                        <div class="col-sm-12">
                                            <input type="file" class="form-control" id="file" name="file" maxlength="50"
                                                onchange="previewFile(this)">
                                        </div>
                                    </div>

                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-primary" id="saveBtn2" value="update">Save changes
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- show modal --}}
                <div class="modal fade bd-example-modal-sm show-modal" tabindex="-1" role="dialog"
                    aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="card" style="width:100%">
                                <img class="card-img-top" style="margin: auto" src="" alt="Card image" style="max-width:200px">
                                <div class="card-body">
                                    <h4 class="card-title">John Doe</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- delete confirm --}}
                <div id="confirmModal" class="modal fade bd-example-modal-sm" role="dialog" ria-labelledby="mySmallModalLabel"
                    aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Confirmation</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">
                                <p align="center" style="margin:0;">Are you sure you want to delete this user?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" name="ok_button" id="ok_button" class="btn btn-danger btn-sm">OK</button>
                                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>

            </body>

            <script type="text/javascript">
                $(document).ready(function() {
                    var table = $('.data-table').DataTable({
                        processing: true,
                        serverSide: true,
                        ajax: "{{ url('customers') }}",
                        columns: [{
                                data: 'DT_RowIndex',
                                name: 'DT_RowIndex'
                            },
                            {
                                data: 'first_name',
                                name: 'first_name'
                            },
                            // {data: 'last_name', name: 'last_name'},
                            {
                                data: 'email',
                                name: 'email'
                            },
                            {
                                data: 'action',
                                name: 'action',
                                orderable: false,
                                searchable: false
                            },
                        ]
                    });

                    $('#createNewCustomer').click(function() {
                        $(".print-error-msg").hide();
                        $('#saveBtn').val("create-Customer");
                        $('#Customer_id').val('');
                        $('#CustomerForm').trigger("reset");
                        $('#modelHeading').html("Add New User");
                        $('#ajaxModel').modal('show');
                    });

                    $('#saveBtn').click(function(e) {
                        e.preventDefault();
                        // $(this).html('Sending..');
                        $.ajax({
                            data: $('#CustomerForm').serialize(),
                            url: "{{ route('customers.store') }}",
                            type: "POST",
                            success: function(data) {

                                if ($.isEmptyObject(data.error)) {
                                    $('#CustomerForm').trigger("reset");
                                    $('#ajaxModel').modal('hide');
                                    table.draw();
                                } else {
                                    printErrorMsg(data.error);
                                }
                            },
                            error: function(data) {
                                console.log('Error:', data);
                            }

                        });
                    });

                    function printErrorMsg(msg) {
                        //alert(msg)
                        $(".print-error-msg").find("ul").html('');
                        $(".print-error-msg").css('display', 'block');
                        $.each(msg, function(key, value) {
                            $(".print-error-msg").find("ul").append('<li>' + value + '</li>');
                        });
                    }
                    $('body').on('click', '.showCustomer', function() {

                        var id = $(this).data('id');
                        $.ajax({
                            url: "/customers/" + id + "/edit",
                            dataType: "json",
                            success: function(data) {
                                //alert('hi');
                                //   $('#modelHeading2').html("Edit Customer");
                                //   $('#saveBtn2').val("edit-user");
                                //$('#modal-show').html("<img src={{ URL::to('/') }}/images/" + data.photo + " width='70' class='img-thumbnail' />");
                                $('.show-modal').modal('show');
                                $('.card-img-top').attr('src', "{{ URL::to('/') }}/images/" + data
                                    .photo + "");
                                $('.card-title').html(data.first_name + " " + data.last_name +
                                    "<br><h6>Email: " + data.email + "</h6>");
                                //   $('#Customer_id2').val(data.id);
                                //   $('#first_name2').val(data.first_name);
                                //   $('#last_name2').val(data.last_name);
                                //   $('#store_image').html("<img src={{ URL::to('/') }}/images/" + data.photo + " width='70' class='img-thumbnail' />");
                            }
                        })
                    });
                    $('body').on('click', '.editCustomer', function() {
                        var id = $(this).data('id');
                        $.ajax({
                            url: "/customers/" + id + "/edit",
                            dataType: "json",
                            success: function(data) {
                                $('#modelHeading2').html("Edit Customer");
                                $('#saveBtn2').val("edit-user");
                                $('#ajaxModel2').modal('show');
                                $('#Customer_id2').val(data.id);
                                $('#first_name2').val(data.first_name);
                                $('#last_name2').val(data.last_name);
                                if (data.photo != null) {
                                    $('#store_image').html(
                                        "<img class='pic' src={{ URL::to('/') }}/images/" +
                                        data
                                        .photo + " width='70' class='img-thumbnail' />");
                                }

                            }
                        })
                    });

                    function previewFile(input) {
                        console.log("hi");
                        var file = $("input[type=file]").get(0).files[0];
                        if (file) {
                            var reader = new FileReader();
                            reader.onload = function() {
                                $('#pic').attr("src", reader.result);
                                //$('#store_image').html("<img src=" +reader.result+ " width='70' class='img-thumbnail' />");

                            }
                            reader.readAsDataURL(file);
                        }
                    }

                    $('body').on('submit', '#CustomerForm2', function(e) {
                        e.preventDefault();
                        //$('#saveBtn2').html('Sending..');
                        var formData = new FormData(this);
                        $.ajax({
                            type: 'POST',
                            url: "{{ route('customers.update') }}",
                            data: formData,
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: (data) => {
                                $('#CustomerForm2').trigger("reset");
                                $('#ajaxModel2').modal('hide');
                                table.draw();
                            },
                            error: function(data) {
                                console.log('Error:', data);
                                $('#saveBtn2').html('Save Changes');
                            }
                        });
                    });

                    $('body').on('click', '.deleteCustomer', function() {

                        var id = $(this).data("id");
                        $('#confirmModal').modal('show');
                        $('#ok_button').click(function() {

                            $.ajax({
                                type: "DELETE",
                                data: {
                                    "_token": "{{ csrf_token() }}"
                                },
                                url: "customers/destroy/" + id,
                                success: function(data) {
                                    table.draw();
                                    $('#confirmModal').modal('hide');
                                },
                                error: function(data) {
                                    console.log('Error:', data);
                                }
                            });
                        });


                    });


                });

            </script>

            </html>
