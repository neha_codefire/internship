<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\Models\Customer;
use Illuminate\Support\Facades\File;
use Validator;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->ajax()) {
            $data = Customer::latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {

                    $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $row->id . '" data-original-title="Show" class="show btn btn-success btn-sm showCustomer">Show</a>&nbsp';
                    
                    $btn = $btn . '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $row->id . '" data-original-title="Edit" class="edit btn btn-primary btn-sm editCustomer">Edit</a>';

                    $btn = $btn . ' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $row->id . '" data-original-title="Delete" class="btn btn-danger btn-sm deleteCustomer">Delete</a>';
                    

                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('Customer');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => ['required','unique:employees'],
            'password' => 'required|confirmed|min:6',
        ]);
     
        if ($validator->passes()) {
            Customer::create($request->all());
            return response()->json(['success'=>'Added new records.']);
        }
        return response()->json(['error'=>$validator->errors()->all()]);

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer  $Customer
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Customer = Customer::find($id);
        return response()->json($Customer);
    }

    public function update(Request $request)
    {
        $id = $request->Customer_id2;
        $userData = Customer::find($id);
        $userData->first_name = request('first_name2');
        $userData->last_name = request('last_name2');
        $image=$request->file('file');
        if($image!=null){
            request()->validate([
                'photo' => 'photo|mimes:jpeg,png,jpg,gif,svg|max:2048',
           ]);
            if(File::exists('images/'.$userData->photo)) {
                File::delete('images/'.$userData->photo);
            }
            $imageName=time().".".$image->extension();
            $image->move(public_path('images'),$imageName);
            $userData->photo=$imageName;
        }
        $userData->save();
        return json_encode(array('statusCode'=>200));   
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $Customer
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $userData = Customer::find($id);
        if(File::exists('images/'.$userData->photo)) {
            File::delete('images/'.$userData->photo);
        }
        $userData->delete();
        return response()->json(['success' => 'Customer deleted!']);
    }
}
