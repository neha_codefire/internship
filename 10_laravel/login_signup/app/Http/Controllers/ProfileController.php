<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UpdateProfileRequest;
use App\Models\User;
use Illuminate\Support\Facades\File;

class ProfileController extends Controller
{
    public function update(UpdateProfileRequest $request)
    {
        // auth()->user()->update($request->only('photo'));
        // if($request->input('password')){
        //     auth()->user()->update([
        //         'password'=>bcrypt($request->input('password'))
        //     ]);
        // }
        // return redirect()->route('profile')->with('message','Profile updated Successfully');



        $id = auth()->user()->id;
        $userData = User::find($id);
        // $userData->first_name = request('first_name2');
        // $userData->last_name = request('last_name2');
        $image=$request->file('file');
        if($image!=null){
            request()->validate([
                'photo' => 'photo|mimes:jpeg,png,jpg,gif,svg|max:2048',
           ]);
            if(File::exists('images/'.$userData->photo)) {
                File::delete('images/'.$userData->photo);
            }
            $imageName=time().".".$image->extension();
            $image->move(public_path('images'),$imageName);
            $userData->photo=$imageName;
        }
        $userData->save();
        return redirect()->route('profile')->with('message','Profile updated Successfully');
    }
}
