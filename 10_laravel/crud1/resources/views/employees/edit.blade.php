@extends('employees.layout')
   
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Update Employee</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('employees.index') }}"> Back</a>
            </div>
            <div>
                <img src="{{asset('images')}}/{{$employee->photo}}" style="max-width: 100px;"/>
            </div>
        </div>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Warning!</strong> Please check input field code<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('employees.update',$employee->id) }}" method="POST" enctype="multipart/form-data" id="create_form">
        @csrf
        @method('PUT')
   
         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>First Name:</strong>
                    <input type="text" name="first_name" value="{{ $employee->first_name }}" class="form-control" placeholder="First Name">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Last Name:</strong>
                    <input type="text" name="last_name" value="{{ $employee->last_name }}" class="form-control" placeholder="Last Name">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <img id="previewimg" alt="Profile Photo" style="max-width: 130px;margin-top:20px;"/><br>
                    <strong>Profile Photo:</strong>
                    <input type="file" name="file"  class="form-control" onchange="previewFile(this)">
                    
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
   
    </form>
@endsection
<script>
    // function previewFile(input){
    //     var file=$("input[type=file]").get(0).files[0];
    //     if(file)
    //     {
    //         var reader=new FileReader();
    //         reader.onload=function(){
    //             $('#previewimg').attr("src",reader.result);
    //         }
    //         reader.readAsDataURL(file);
    //     }
    // }
</script>