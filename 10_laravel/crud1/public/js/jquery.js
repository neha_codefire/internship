$(document).ready(function(){
    $("#create_form").validate({
        ignore:[],
        rules:{
            first_name:{
                required: true,
                maxlength: 20,
                minlength: 4,
            },
            last_name:{
                required: true,
                maxlength: 20,
                minlength: 4,
            },
            email:{
                required: true,
                email: true,
                maxlength: 50
            },
            password:{
                required: true,
                minlength: 6,
            },
            password_confirmation:{
                required: true,
                equalTo: "#password",
            }

        },
        messages:{

        },
    })
});
    // function previewFile(input){
    //     var file=$("input[type=file]").get(0).files[0];
    //     if(file)
    //     {
    //         var reader=new FileReader();
    //         reader.onload=function(){
    //             $('#previewimg').attr("src",reader.result);
    //         }
    //         reader.readAsDataURL(file);
    //     }
    // }