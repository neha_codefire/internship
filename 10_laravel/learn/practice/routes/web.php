<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Users;
use App\Http\Controllers\form;
use App\http\Controllers\db_eg;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//passing variable in url--------
// Route::get('/{name}', function ($name) {
//     echo $name;
//     return view('hello',['name'=>$name]);
// });
//changing route------------
// Route::get('/about',function(){
//     return view('about');
// });
//other way of routing---------------
Route::view('about','about');
Route::view('xyz','contact');
//default routing--------
// Route::get('/', function () {
//     return view('welcome');
// });
//to redirect default to another page
Route::get('/', function () {
    //return view('welcome');
    return redirect('about');
});

//Route::get("path","Controller file");
//Route::get("users",[Users::class,'index']);
//send data from here
//Route::get("users{user}",[Users::class,'index']);

//Route::get("users",[Users::class,'loadView']);
//Route::get("users",[Users::class,'loadView']);

Route::get("users",[Users::class,'viewLoad']);

Route::post("form",[form::class,'getData']);
Route::view("form","form");
Route::view("home","home");
Route::view("noaccess","noaccess");
Route::get("db",[db_eg::class,'index']);