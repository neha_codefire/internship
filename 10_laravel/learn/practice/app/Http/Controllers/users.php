<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class users extends Controller
{
    //
    // function index($user)
    // {
    //     // echo $user;
    //     // echo " hello from controller";

    //     //to use controller as api
    //     return['name'=>"neha",'age'=>27];
    // }
    // public function loadView()
    // {
    //     //return view("hello");
    //     return view("hello",['name'=>"anil sidhu"]);
    // }

    public function viewLoad()
    {
        //return view('users');
        return view('users',['user'=>['anil','sam','peter']]);
    }

}
