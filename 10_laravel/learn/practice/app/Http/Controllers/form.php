<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class form extends Controller
{
    function getData(Request $req)
    {
        $req->validate([
            'first_name'=>'required | min:2',
            'last_name'=>'required',
            'email'=>'required',
            'password'=>'required | min:5',
            'confirm_password'=>'required'

        ]);
        return $req->input();
    }
}
