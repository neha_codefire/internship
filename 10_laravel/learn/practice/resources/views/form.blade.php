<h1>Add New User</h1>
{{$errors}}
<h3>List of errors</h3>
@if($errors->any())
@foreach($errors->all() as $err)
<li>{{$err}}</li>@endforeach
@endif
<form action="form" method="POST">
    @csrf
    <input type="text" name="first_name" placeholder="Enter First Name"/>
    <span style="color:red;">@error('first_name'){{$message}}@enderror</span><br><br>
    <input type="text" name="last_name" placeholder="Enter Last Name"/>
    <span style="color:red;">@error('last_name'){{$message}}@enderror</span><br><br>
    <input type="text" name="email" placeholder="Enter user Email"/>
    <span style="color:red;">@error('email'){{$message}}@enderror</span><br><br>
    <input type="password" name="password" placeholder="Enter user password"/>
    <span style="color:red;">@error('password'){{$message}}@enderror</span><br><br>
    <input type="password" name="confirm_password" placeholder="Confirm"/>
    <span style="color:red;">@error('confirm_password'){{$message}}@enderror</span><br><br>
    <button type="submit">ADD User</button>
</form>