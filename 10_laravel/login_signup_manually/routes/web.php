<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;//for password reset
//its a built in password broker to send reset link to the user
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//opens the laravel home page
Route::get('/', function () {
    return view('welcome');
});

//opens registration form
Route::get('/register',[App\Http\Controllers\RegistrationController::class,'create'])->name('register');

//regitration form submit
Route::post('/register',[App\Http\Controllers\RegistrationController::class,'store']);

//opns login form
Route::get('/login',[App\Http\Controllers\SessionController::class,'create'])->name('login');

//login form submit
Route::post('/login',[App\Http\Controllers\SessionController::class,'store']);

//logout
Route::get('/logout',[App\Http\Controllers\SessionController::class,'destroy'])->name('logout');

//opens dashboard only if user is logged in
Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware('verified');

//routes to update profile
//opens update profile page only if user is logged in
Route::get('/profile', function () {
        return view('profile');
})->middleware('verified')->name('profile');

//updates profile opens update profile page only if user is logged in
Route::post('/profile',[\App\Http\Controllers\ProfileController::class,'update'] )->middleware('verified')->name('profile.update');

//Routes for email verfication
//opens after user registration
Route::get('/email/verify', function () {
    return view('auth.verify-email');
})->middleware('auth')->name('verification.notice');

//when user verifies the email the redirected to login page
Route::get('/email/verify/{id}/{hash}', function (EmailVerificationRequest $request) {
    $request->fulfill();
    return redirect('/login')->with('message', 'Account has been successfully verified!');
})->middleware(['auth', 'signed'])->name('verification.verify');


Route::post('/email/verification-notification', function (Request $request) {
    $request->user()->sendEmailVerificationNotification();
    return back()->with('message', 'Verification link sent!');
})->middleware(['auth', 'throttle:6,1'])->name('verification.send');

//routes for password reset
//opens password reset form
Route::get('/forgot-password', function () {
    return view('auth.forgot-password');
})->middleware('guest')->name('password.request');

//for sending password reset link
Route::post('/forgot-password', function (Request $request) {
    $request->validate(['email' => 'required|email']);

    //Password broker via Password facade to send password reset link
    $status = Password::sendResetLink(
        $request->only('email')
    );

    return $status === Password::RESET_LINK_SENT
                ? back()->with(['status' => __($status)])
                : back()->withErrors(['email' => __($status)]);
})->middleware('guest')->name('password.email');

//opens reset password form when user clicks on verification link
Route::get('/reset-password/{token}', function ($token) {
    return view('auth.reset-password', ['token' => $token]);
    //token is used to verify password reset link which will be used later
})->middleware('guest')->name('password.reset');

//reset password form submission
Route::post('/reset-password', function (Request $request) {
    $request->validate([
        'token' => 'required',
        'email' => 'required|email',
        'password' => 'required|min:8|confirmed',
    ]);

    //use of password broker to validate the password reset request
    //if all the values are valid then reset method will be invoked within the closure
    //this method receives user instance and plain text password entered in the form
    //it will update the password in db
    $status = Password::reset(
        $request->only('email', 'password', 'password_confirmation', 'token'),
        function ($user, $password) use ($request) {
            $user->forceFill([
                'password' => Hash::make($password)
            ])->setRememberToken(Str::random(60));

            $user->save();

            event(new PasswordReset($user));
        }
    );

    return $status == Password::PASSWORD_RESET
                ? redirect()->route('login')->with('status', __($status))
                : back()->withErrors(['email' => [__($status)]]);
})->middleware('guest')->name('password.update');
