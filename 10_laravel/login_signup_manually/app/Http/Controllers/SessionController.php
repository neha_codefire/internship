<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class SessionController extends Controller
{
    public function create()
    {
        Auth::logout();
        return view('sessions.create');
    }
    public function store(Request $request)
    {
        // $this->validate(request(), [
        //     'email' => 'required',
        //     'password' => 'required'
        // ]);
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            if(Auth::user()->email_verified_at == null){
                Auth::logout();
                return back()->withErrors([
                    'email' => 'Please Verify your email first.',
                    ]);
            }
            return redirect()->to('dashboard')->with('success','Logged in Successfully');
            $request->session()->regenerate();

            //return redirect()->to('dashboard');
        }

        return back()->withErrors([
            'email' => 'The provided credentials do not match our records.',
        ]);
    }
    public function destroy(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();
    
        $request->session()->regenerateToken();
    
        return redirect('/');
        // auth()->logout();
        
        // return redirect()->to('/login');
    }

}
