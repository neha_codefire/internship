<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'name'=>'min:2',
            // 'email'=>'email',
            // 'password'=>'min:8|confirmed',
            'photo' => 'photo|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];
    }
}
