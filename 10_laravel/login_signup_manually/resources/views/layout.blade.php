<!DOCTYPE html>
<html>

<head>
  <title>Main layout page </title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>

<body>
  <!-- <div class="jumbotron text-center" style="height:100px">
  <h1></h1> 
</div> -->
  <nav class="navbar navbar-inverse">
    <div class="container-fluid">
      <div class="navbar-header">
        <a class="navbar-brand" href="#">Travel Ninja</a>
      </div>
      <ul class="nav navbar-nav">
        @if( auth()->check() )
        <li class="active"><a href="/dashboard">Dashboard</a></li>
        @endif
        <!-- <li><a href="#">Page 1</a></li>
      <li><a href="#">Page 2</a></li> -->
      </ul>
      <ul class="nav navbar-nav navbar-right">
        @if( auth()->check() )
        <?php
        if(auth()->user()->photo!=null){
                              ?> <img src="{{asset('images')}}/{{Auth::user()->photo}}" height="50" width="50" style="border-radius:50px;padding:4px;margin-right:4px;"/>
                                <?php
                            }
                            else{
                                ?>
                                <img src="{{asset('images')}}/default.png" height="50" width="50" style="border-radius:50%;padding:4px;margin-right:4px;"/>
                                <?php
                            }
                            ?>
        <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">{{ auth()->user()->name }}
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="/profile">Edit Profile</a></li>
          <li><a href="/logout">Log Out</a></li>
        </ul>
      </li>
        @else
        <li>
          <a href="/login">Log In</a>
        </li>
        <li>
          <a href="/register">Register</a>
        </li>
        @endif
        <!-- <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
      <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li> -->
      </ul>
    </div>
  </nav>


  <div class="container">
    @yield('content')
  </div>
  </div>
</body>

</html>