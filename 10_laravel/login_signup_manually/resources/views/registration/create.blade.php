@extends('guest-layout')
@section('content')
<h2>Register</h2>
<form method="POST" action="/register">
    {{ csrf_field() }}
    <div class="form-group">
        <label for="name">Name:</label>
        <input type="text" class="form-control" id="name" name="name" value="{{old('name')}}">
        <span style="color:red">@error('name'){{$message}}@enderror</span>
    </div>

    <div class="form-group">
        <label for="email">Email:</label>
        <input type="email" class="form-control" id="email" name="email" value="{{old('email')}}">
        <span style="color:red">@error('email'){{$message}}@enderror</span>
    </div>

    <div class="form-group">
        <label for="password">Password:</label>
        <input type="password" class="form-control" id="password" name="password">
        <span style="color:red">@error('password'){{$message}}@enderror</span>
    </div>

    <div class="form-group">
        <label for="password_confirmation">Password Confirmation:</label>
        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation">
        <span style="color:red">@error('password_confirmation'){{$message}}@enderror</span>
    </div>

    <div class="form-group">
        <button style="cursor:pointer" type="submit" class="btn btn-primary">Submit</button>
        <!-- <span><a href="{{route('login')}}">Already Registered</a></span> -->
    </div>
</form>
@endsection