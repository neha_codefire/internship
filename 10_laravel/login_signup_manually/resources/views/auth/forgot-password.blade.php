@extends('guest-layout')
@section('content')
<h2>Forgot Password</h2>
@if($errors->any())
<h5 style="color:red">{{$errors->first()}}</h5>
@endif
@if(session('status'))
<div class="alert alert-success alert-dismissible">
<button type="button" class="close" data-dismiss="alert">&times;</button>
    {{ session('status') }}
</div>
@endif
<form method="POST" action="{{route('password.email')}}">
    {{ csrf_field() }}
    <div class="form-group">
        <label for="email">Email:</label>
        <input type="email" class="form-control" id="email" name="email" value="{{old('email')}}">
        <!-- <span style="color:red">@error('email'){{$message}}@enderror</span> -->
    </div>

    <div class="form-group">
        <button style="cursor:pointer" type="submit" class="btn btn-primary">Send Link</button>

    </div>
</form>
</body>

</html>
@endsection