@extends('guest-layout')
@section('content')

@if(session('message'))
<div class="alert alert-success alert-dismissible">
<button type="button" class="close" data-dismiss="alert">&times;</button>
    {{ session('message') }}
</div>
@endif
<div class="jumbotron">
<h2>Thanks for signing up!</h2>
<p class="lead"> Before getting started, could you verify your email address by clicking on the link we just emailed to you? If you didn't receive the email, we will gladly send you another.
</p>
<p>A new verification link has been sent to the email address you provided during registration.
</p>
<form class="d-inline" method="POST" action="{{ route('verification.send') }}">
    @csrf
    <div class="form-group">
        <button type="submit" class="btn btn-primary">
            Resend Verification Email
        </button>
    </div>
</form>
</div>
@endsection