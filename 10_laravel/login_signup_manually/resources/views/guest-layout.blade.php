<!DOCTYPE html>
<html>

<head>
  <title>Main layout page </title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>

<body>
  <!-- <div class="jumbotron text-center" style="height:100px">
  <h1></h1> 
</div> -->
  <nav class="navbar navbar-inverse">
    <div class="container-fluid">
      <div class="navbar-header">
        <a class="navbar-brand" href="#">Travel Ninja</a>
      </div>
      <ul class="nav navbar-nav navbar-right">
        <li>
          <a href="/login">Log In</a>
        </li>
        <li>
          <a href="/register">Register</a>
        </li>
      </ul>
    </div>
  </nav>


  <div class="container">
    @yield('content')
  </div>
  <!-- </div> -->
</body>

</html>