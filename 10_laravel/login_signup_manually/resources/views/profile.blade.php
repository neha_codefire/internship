@extends('layout')
@section('content')

<div class="jumbotron">
  <h3 class="display-4">Update Profile</h3>

  <?php
                if (auth()->user()->photo != null) {
                ?> <img src="{{asset('images')}}/{{auth()->user()->photo}}" style="max-width: 100px;margin-bottom:20px" />
                <?php
                } else {
                ?>
                    <img src="{{asset('images')}}/default.png" style="max-width: 100px;margin-bottom:20px" />
                <?php
                }
                ?>

                <form method="POST" action="{{route('profile.update')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="file">Update Profile Photo</label>
                        <input type="file" class="form-control" id="file" name="file" autofocus>
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
  <!-- <p class="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
  <hr class="my-4">
  <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
  <a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a> -->
</div>

                

@endsection