@extends('guest-layout')
@section('content')
<h2>Log In</h2>
@if($errors->any())
<h5 style="color:red">{{$errors->first()}}</h5>
@endif
@if(session('message'))
<div class="alert alert-success alert-dismissible">
<button type="button" class="close" data-dismiss="alert">&times;</button>
    {{ session('message') }}
</div>
@endif
@if(session('status'))
<div class="alert alert-success alert-dismissible">
<button type="button" class="close" data-dismiss="alert">&times;</button>
    {{ session('status') }}
</div>
@endif
<form method="POST" action="/login">
    {{ csrf_field() }}
    <div class="form-group">
        <label for="email">Email:</label>
        <input type="email" class="form-control" id="email" name="email" value="{{old('email')}}">
        <!-- <span style="color:red">@error('email'){{$message}}@enderror</span> -->
    </div>

    <div class="form-group">
        <label for="password">Password:</label>
        <input type="password" class="form-control" id="password" name="password">
        <!-- <span style="color:red">@error('password'){{$message}}@enderror</span> -->
    </div>

    <div class="form-group">
        <button style="cursor:pointer" type="submit" class="btn btn-primary">Login</button>

    </div>
</form>
<span><a href="{{route('password.request')}}">Forgot password?</a></span>
</body>

</html>
@endsection