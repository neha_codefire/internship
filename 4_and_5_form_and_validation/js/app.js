$(document).ready(function () {


    let fnameError = true;
    let lnameError = true;
    let phoneError = true;
    let emailError = true;
    let dateError = true;
    let genderError = true;
    let pwdError = true;
    let cpwdError = true;
    let fileError = true;
    let qualError = true;


    //first name
    function validateFirstName() {
        let fname = $('#first-name').val();
        if (fname.trim().length == '') {
            $('#fnameError').text("First Name is missing");
            fnameError = false;
            return false;
        }
        else if ((fname.trim().length < 3) || (fname.trim().length > 10)) {
            $("#fnameError").text("length of First must be between 3 and 10");
            fnameError = false;
            return false;
        }
        else {
            $("#fnameError").hide();
        }
    }

    //last name
    function validateLasttName() {
        let lname = $('#last-name').val();
        if (lname.trim().length == '') {
            $('#lnameError').text("Last Name is missing");
            lnameError = false;
            return false;
        }
        else if ((lname.trim().length < 3) || (lname.trim().length > 10)) {
            $("#lnameError").text("length of lastname must be between 3 and 10");
            fnameError = false;
            return false;
        }
        else {
            $("#lnameError").hide();
        }
    }

    //phone number
    function validatePhoneNumber() {
        let phone = $('#phone-number').val();
        let regxPhone = /^[7-9]\d{9}$/;
        if (phone.trim().length == '') {
            $('#phoneError').text("Phone number is missing");
            phoneError = false;
            return false;
        }
        else if (phone.length != 10) {
            $("#phoneError").text("Invalid phone number");
            phoneError = false;
            return false;
        }
        else if (!regxPhone.test(phone)) {
            $("#phoneError").text("Invalid phone number format");
            phoneError = false;
            return false;
        }
        else {
            $("#phoneError").hide();
        }
    }

    //email
    function validateEmail() {
        let email = $('#email').val();
        let regxEmail = /^([a-zA-Z0-9\.-]+)@([a-zA-Z0-9-]+).([a-z]{2,8})(.[a-z]{2,8})?$/;
        if (email.trim().length == '') {
            $('#emailError').text("Email is missing");
            emailError = false;
            return false;
        }
        else if (!regxEmail.test(email)) {
            $("#emailError").text("Invalid email address");
            emailError = false;
            return false;
        }
        else {
            $("#emailError").hide();
        }
    }

    //Date
    function validateDate() {
        let date = $('#date').val();
        if (date.trim().length == '') {
            $('#dateError').text("date is missing");
            emailError = false;
            return false;
        }
        else {
            $("#dateError").hide();
        }
    }

    //Gender
    function validateGender() {
        var gender = $("input[type = 'radio']:checked");
        if (gender.length == 0) {
            $('#genderError').text("Select gender");
            return false;
        }
        else {
            $("#genderError").hide();
        }
    }
    //Qualification
    function validateQualification() {
        var qual = $('#qualification');
        if (qual.length == 0 || qual.val() == "") {
            $('#qualError').text("Select Qualification");
            return false;
        }
        else {
            $("#qualError").hide();
        }

    }

    //password
    function validatePwd() {
        let pwd = $('#pwd').val();
        let cpwd = $('#cpwd').val();
        let regxPass = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;
        if (pwd.trim().length == '') {
            $('#pwdError').text("Password is missing");
            emailError = false;
            return false;
        }
        else if (!regxPass.test(pwd)) {
            $("#pwdError").text("Min length 8 characters<br>must contain atleast one digit,one upper case alphbet,one lower case alphabet and atleast 1 character:!@#$%&*()-+=^<br>it doesn't contain any white space");
            pwdError = false;
            return false;
        }
        else {
            $("#pwdError").hide();
        }
    }

    // //Confirm password
    function validateCpwd() {
        let cpwd = $('#cpwd').val();
        let pwd = $('#pwd').val();
        if (cpwd.trim().length == '') {
            $('#cpwdError').text("Enter password to confirm");
            cpwdError = false;
            return false;
        }
        else if (cpwd != pwd) {
            $('#cpwdError').text("Password doesn't match");
            cpwdError = false;
            return false;
        }
        else {
            $("#cpwdError").hide();
        }
    }

    //File
    function validateFile() {
        let file = $(":file").val();
        if (file == "") {
            $('#fileError').text("Upload Resume");
            fileError = false;
            return false;
        }
        else {
            $("#fileError").hide();
        }
    }


    //submit
    $('#submitbtn').click(function () {
        validateFirstName();
        validateLasttName();
        validatePhoneNumber();
        validateEmail();
        validateDate();
        validateGender();
        validateQualification()
        validatePwd();
        validateCpwd();
        validateFile();

        if ((fnameError == true && lnameError == true && phoneError == true && emailError == true && dateError == true && genderError == true && qualError == true && pwdError == true && fileError == true && cpwdError == true)) {
            return true;
        }
        else {
            return false;
        }
    });

    // $("#first-name").keyup(validateFirstName);
    // $("#last-name").keyup(validateLasttName);
    // $("#phone-number").keyup(validatePhoneNumber);
    // $("#email").keyup(validateEmail);
    // $("#date").keyup(validateDate);
    // $(".gender").keyup(validateGender);
    // $("#pwd").keyup(validatePwd);
    // $("#cpwd").keyup(validateCpwd);
    // $("#file").keyup(validateFile);


});
