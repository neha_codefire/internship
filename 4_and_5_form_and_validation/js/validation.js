$(document).ready(function(){
    
    //first name
    let fnameError=true;
    // $('#first-name').keyup(function(){
    //     validateFirstName();
    // });

    function validateFirstName(){
        let fname=$('#first-name').val();
        if(fname.trim().length==''){
            $('h5').("First Name is missing");
            fnameError=false;
            return false;
        }
        else if((fname.trim().length<3) || (fname.trim().length>10)){
            $("#fnameError").text("length of username must be between 3 and 10");
            fnameError=false;
            return false;
        }
        else{
            $("#fnameError").hide()
        }
    }


    //submit
    $('#submitbtn').click(function(){
        validateFirstName();
        if((fnameError==true)){
            return true;
        }
        else{
            return false;
        }
    });

});