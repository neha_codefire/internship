<?php
include 'include/connection.php';
$limit_per_page = 5;
if (isset($_POST["page_no"])) {
    $page = $_POST["page_no"];
} else {
    $page = 1;
}
$offset = ($page - 1) * $limit_per_page;
$output = "";
$sql = "SELECT * FROM users ORDER BY id LIMIT {$offset},{$limit_per_page}";
$result = $conn->query($sql);

if (mysqli_num_rows($result) > 0) {
    $output = '<table class="table table-striped table-hover table-bordered">
    <thead class="thead-dark">
    <tr>
        <th>Name</th>
        <th>Email ID</th>
        <th>View</th>
        <th>Edit</th>
        <th>Delete</th>
    </tr>
    </thead>';
    while ($row = $result->fetch_assoc()) {
        $output .= "    <tr>
        <td>{$row["first_name"]}  {$row["last_name"]}</td>
        <td>{$row["email"]}</td>
        <td><button class='viewUser btn btn-primary' data-id='{$row["id"]}'>View</button></td>
        <td><button class='editUser btn btn-warning' data-id='{$row["id"]}'>Edit</button></td>
        <td><button class='deleteUser btn btn-danger' data-id='{$row["id"]}' data-page='{$page}'>Delete</button></td>
    </tr>";
    }
    $output .= '</table>';
    $sql_total = "SELECT * FROM users";
    $records = $conn->query($sql_total);
    $total_records = mysqli_num_rows($records);
    $total_pages = ceil($total_records / $limit_per_page);
    $output .= '<div id="pagination" class="pagination justify-content-center pagination-sm m-0">';
    for ($i = 1; $i <= $total_pages; $i++) {
        if ($i == $page) {
            $class_name = "active";
        } else {
            $class_name = "";
        }
        $output .= "<a class='{$class_name} page-link' id='{$i}' href=''>{$i}</a>";
    }
    $output .= '</div>';


    echo $output;
} else {
    echo "<h2>No record found.</h2>";
}
