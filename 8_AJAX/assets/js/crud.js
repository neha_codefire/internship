$(document).ready(function () {
    //for hiding update user modal
    $("#update-msg").hide();
    //to load all the users in home page
    function load_data(page) {
        $.ajax({
            url: "retrieve.php",
            type: "POST",
            data: {
                page_no: page
            },
            success: function (data) {
                $('#table-data').html(data);
            },
            error: function (data) {
                alert("error");
            }
        });

    }
    load_data();
    //pagination code
    $(document).on('click', '#pagination a', function (e) {
        e.preventDefault();
        var page_id = $(this).attr("id");
        load_data(page_id);
    });
    //update User
    $(document).on('click', '#photo-submit', function () {
        //var form = $(this).get('#pic-form');
        var formData = new FormData();
        //var fd = new FormData();
        var files = $('#image')[0].files;
        var id = $(this).data("id");
        var fname = $("#fname").val();
        var lname = $("#lname").val();
        // Check file selected or not
        if (files.length > 0) {
            formData.append('file', files[0]);
            formData.append('id', id);
            formData.append('fname', fname);
            formData.append('lname', lname);
        }
        else {
            formData.append('id', id);
            formData.append('fname', fname);
            formData.append('lname', lname);
        }
        $.ajax({
            url: "updateUser.php",
            type: "POST",
            data: formData,
            contentType: false,
            cache: false,
            processData: false,//we are sending file
            success: function (data) {
                if (data == 1) {
                    $("#update-msg").show().delay(2000).fadeOut();
                    load_data();
                    $("#editModal").hide();
                }
                else {
                    $("#update-msg").html("No");
                }
            }

        })
    });
    //delete user
    $(document).on("click", ".deleteUser", function () {
        var element = this;
        var id = $(this).data("id");
        var page = $(this).data("page");
        $.ajax({
            url: "delete.php",
            type: "post",
            data: { id: id },
            success: function (data) {
                if (data == 1) {
                    $(element).closest("tr").fadeOut();
                    load_data(page);
                }
            }

        });
    });
    //edit user
    $(document).on("click", ".editUser", function (e) {
        $("#editModal").show();
        var id = $(this).data("id");
        $.ajax({
            url: "edit.php",
            type: "POST",
            data: { id: id },
            success: function (data) {
                $("#edit-user table").html(data);
            }
        })


    });
    //close modal boxes
    $(".close-btn").on("click", function () {
        $("#editModal").hide();
        $("#viewModal").hide();
    });
    //View User Profile
    $(document).on("click", ".viewUser", function () {
        $("#viewModal").show();
        var id = $(this).data("id");
        $.ajax({
            url: "view.php",
            type: "post",
            data: { id: id },
            success: function (data) {
                $("#view-user").html(data);
            }

        });
    });
    //
    //
    //
    //form Validation Code
    let fnameError = true;
    let lnameError = true;
    let emailError = true;
    let pwdError = true;
    let cpwdError = true;
    let fileError = true;
    let regxname = /^[a-zA-Z-' ]*$/;
    let regxPass = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;
    //first name
    function validateFirstName() {
        let fname = $('#first-name').val();
        if (fname.trim().length == '') {
            $('#fnameError').text('First Name is missing');
            fnameError = false;
            return false;
        }
        else if ((fname.trim().length < 3) || (fname.trim().length > 10)) {
            $("#fnameError").text("length of First must be between 3 and 10");
            fnameError = false;
            return false;
        }
        else if (!regxname.test(fname)) {
            $("#fnameError").text("Only letters and white space allowed");
            fnameError = false;
            return false;
        }
        else {
            $("#fnameError").text("");
        }
    }
    //last name
    function validateLastName() {
        let lname = $('#last-name').val();
        if (lname.trim().length == '') {
            $('#lnameError').text("Last Name is missing");
            lnameError = false;
            return false;
        }
        else if ((lname.trim().length < 3) || (lname.trim().length > 10)) {
            $("#lnameError").text("length of lastname must be between 3 and 10");
            fnameError = false;
            return false;
        }
        else if (!regxname.test(lname)) {
            $("#fnameError").text("Only letters and white space allowed");
            fnameError = false;
            return false;
        }
        else {
            $("#lnameError").text("");
        }
    }
    //email
    function validateEmail() {
        let email = $('#email').val();
        let regxEmail = /^([a-zA-Z0-9\.-]+)@([a-zA-Z0-9-]+).([a-z]{2,8})(.[a-z]{2,8})?$/;
        if (email.trim().length == '') {
            $('#emailError').text("Email is missing");
            emailError = false;
            return false;
        }
        else if (!regxEmail.test(email)) {
            $("#emailError").text("Invalid email address");
            emailError = false;
            return false;
        }
        else {
            $("#emailError").text("");
        }
    }
    //password
    function validatePwd() {
        let pwd = $('#pwd').val();
        if (pwd.trim().length == '') {
            $('#pwdError').text("Password is missing");
            emailError = false;
            return false;
        }
        else if (!regxPass.test(pwd)) {
            $("#pwdError").text("Min length 8 characters | must contain atleast one digit,one upper case alphbet,one lower case alphabet and atleast 1 character:!@#$%&*()-+=^ | it doesn't contain any white space");
            pwdError = false;
            return false;
        }
        else {
            $("#pwdError").text("");
        }
    }
    //Confirm password
    function validateCpwd() {
        let cpwd = $('#cpwd').val();
        let pwd = $('#pwd').val();
        if (cpwd.trim().length == '') {
            $('#cpwdError').text("Enter password to confirm");
            cpwdError = false;
            return false;
        }
        else if (cpwd != pwd) {
            $('#cpwdError').text("Password doesn't match");
            cpwdError = false;
            return false;
        }
        else {
            $("#cpwdError").text("");
        }
    }
    //file validation
    function validateFile() {
        let file = $(":file").val();
        if (file == "") {
            $('#fileError').text("No photo Selected");
            fileError = false;
            return false;
        }
        else {
            $("#fileError").hide();
        }
    }
    //validation on form submit
    $('#submitbtn').click(function (e) {
        e.preventDefault();
        validateFirstName();
        validateLastName();
        validateEmail();
        validatePwd();
        validateCpwd();
        if ((fnameError == true && lnameError == true && emailError == true && pwdError == true && cpwdError == true)) {
            let fname = $('#first-name').val();
            let lname = $('#last-name').val();
            let email = $('#email').val();
            let pwd = $('#pwd').val();
            let cpwd = $('#cpwd').val();
            // console.log(fname);
            // console.log(lname);
            // console.log(email);
            // console.log(pwd);
            mydata = {
                fname: fname,
                lname: lname,
                email: email,
                pwd: pwd,
                cpwd: cpwd
            };
            //console.log(JSON.stringify(mydata));
            $.ajax({
                url: 'add.php',
                method: 'POST',
                data: JSON.stringify(mydata),//data converted to string
                success: function (data) {
                    msg = "<div class='alert mt-3'>" + data + "</div>";
                    $("#msg").html(msg);
                    $('#myform')[0].reset();
                },
            })

        }


    });
    //validation for update user form submit
    $('#submit3').click(function () {
        validateFile();
        if (fileError == true) {
            return true;
        }
        else {
            return false;
        }
    });
    $('#submit1').click(function () {
        validateFirstName();
        if (fnameError == true) {
            return true;
        }
        else {
            return false;
        }
    });
    $('#submit2').click(function () {
        validateLastName();
        if (lnameError == true) {
            return true;
        }
        else {
            return false;
        }
    });
    // add user form validation ends here
});