<?php
include 'include/connection.php';
/*
php://input - is a read only stream that allows to read raw data from the request body class="it returns all the raw data after http-header of request
regardleass of content type

json_decode- takes json string and converts it into a php object or array, if true then associative array
*/
$data = stripslashes(file_get_contents("php://input"));
$mydata = json_decode($data, true);
//$fname = $lname = $email = $pwd = $cpwd = "";
$fname = test_input($mydata['fname']);
$lname = test_input($mydata['lname']);
$email = test_input($mydata['email']);
$pwd = test_input($mydata['pwd']);
$cpwd = test_input($mydata['cpwd']);
// echo $lname . " " . $fname . " " . $email . " " . $pwd . " " . $cpwd;
//echo "fname=" . $mydata['fname'];
if (!empty($fname) && !empty($lname) && !empty($email) && !empty($pwd) && !empty($cpwd)) {
    $sql = "SELECT email FROM users WHERE email='$email'";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        echo "<span class='text-danger'>User Already Exists</span>";
    } else {
        $stmt = $conn->prepare("INSERT INTO users(first_name,last_name,email,pwd,cpwd)VALUES(?,?,?,?,?)");
        $stmt->bind_param("sssss", $fname, $lname, $email, $pwd, $cpwd);
        // $fname = test_input($_POST["fname"]);
        // $lname = test_input($_POST["lname"]);
        // $pwd = test_input($_POST["pwd"]);
        // $cpwd = test_input($_POST["cpwd"]);
        $stmt->execute();
        //echo "success";
        echo "<span class='text-success'>User added successfully</span>";
        $stmt->close();
        $conn->close();
    }
} else {
    echo "<span class='text-danger'>Failed to update user</span>";
}

function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
