<?php
include 'include/connection.php';
$id = $_POST['id'];
$sql = "SELECT * FROM users WHERE id='$id'";
$result = $conn->query($sql);
$output = "";
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        if ($row['photo'] != null) {
            $output .= "
            <img class='card-img-top' src='assets/images/{$row['photo']}' style='width:100%`;height:340px'>";
        } else {
            $output .= "
            <img class=' card-img-top' src='assets/images/default.png' alt='Card image' style='width:100%'>";
        }
        $output .= "
            <div class='card-body'>
                <h4 class='card-title'>{$row["first_name"]} {$row["last_name"]}</h4>
                <p class='card-text'>{$row["email"]}</p>";
    }
    echo $output;
} else {
    echo "<h2>No found</h2>";
}

$conn->close();
