<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    include 'include/head.php';
    include 'include/connection.php';
    ?>
    <link rel="stylesheet" href="assets/css/page.css">
</head>

<body>
    <div class="container-fluid  mx-auto m-4">
        <?php include 'include/header.php'; ?>
        <!-- Add User popup -->
        <div>
            <button type="button" class="btn btn-success float-right m-4 p-2" data-toggle="modal" data-target="#exampleModal">Add User</button>
        </div>
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add User</h5>
                        <button type="button" class="close" id="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container">
                            <form name="myform" id="myform">
                                <div class="form-group">
                                    <label for="first-name">First Name(required)<large class="text-danger">*</large></label>
                                    <input type="text" class="form-control" name="fname" id="first-name" placeholder="First-Name">
                                    <span class="Error" id="fnameError"></span>
                                </div>
                                <div class="form-group">
                                    <label for="last-name">Last Name(required)<large class="text-danger">*</large></label>
                                    <input type="text" class="form-control" name="lname" id="last-name" placeholder="Last-Name"><span class="Error" id="lnameError"></span>
                                </div>
                                <div class="form-group">
                                    <label for="email">Email(required)<large class="text-danger">*</large></label>
                                    <input type="email" class="form-control" name="email" id="email" placeholder="Enter email"><span class="Error" id="emailError"></span>
                                </div>
                                <div class="form-group">
                                    <label for="pwd">Password(required)<large class="text-danger">*</large></label>
                                    <input type="password" class="form-control" id="pwd" name="pwd" placeholder="Enter password">
                                    <span class="Error" id="pwdError"></span>
                                </div>
                                <div class="form-group">
                                    <label for="cpwd">Confirm Password(required)<large class="text-danger">*</large></label>
                                    <input type="password" class="form-control" id="cpwd" name="cpwd" placeholder="Enter password">
                                    <span class="Error" id="cpwdError"></span>
                                </div>
                                <center><button type="submit" class="btn btn-primary " id="submitbtn">Submit</button></center>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Add user popup ends here -->
        <h2>All Records</h2>
        <div id="table-data">

        </div>
        <!-- edit user -->
        <div id="editModal" class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Update User</h5>
                        <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" id="edit-user">
                        <table id="uuu">

                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!-- View user -->
        <div id="viewModal" class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">User Profile</h5>
                        <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <div class="card" id="view-user" style="width:300px;margin:auto">

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success close-btn" data-dismiss="modal">Back</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<script src="assets/js/jquery-3.6.0.min.js"></script>
<script type="text/javascript" src="assets/js/crud.js"></script>
<!-- <script type="text/javascript" src="assets/js/add.js"></script> -->

</html>