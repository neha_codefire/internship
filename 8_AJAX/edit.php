<?php
include 'include/connection.php';
$id = $_POST['id'];
$sql = "SELECT * FROM users WHERE id='$id'";
$result = $conn->query($sql);
$output = "";
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        if ($row['photo'] != null) {
            $output .= "
            <tr>
                <td colspan='3' align='center'>
                <img id='pic' width='225px' height='228px' class='my-3' src='assets/images/{$row["photo"]}'>
                </td>
            </tr>";
        } else {
            $output .= "
                <tr>
                    <td colspan='2' align='center'>
                    <img id='pic' class='my-3' src='assets/images/default.png'>>
                    </td>
                </tr>";
        }
        $output .= "
        <form enctype='multipart/form-data' id='pic-form' method='post' action=''>
            <tr>
                <td class='form-check-label w-25'>Profile Pic</td>
                <td><input type='file' name='image' id='image' class='form-control' data-default-file='' value='{$row["photo"]}'></td>    
            </tr>
            <tr>
                <td class='form-check-label w-25'>First Name</td>
                <td><input type='text' id='fname' class='form-control mt-1' value='{$row["first_name"]}'></td>
            </tr>
            <tr>
                <td class='form-check-label w-25'>Last Name</td>
                <td><input type='text' id='lname' class='form-control mt-1' value='{$row["last_name"]}'></td>
                
        </tr>
        <tr>
        <td align='center' colspan='2'><input type='submit' id='photo-submit' name='pic' value='UPDATE' data-id='{$row["id"]}' class='btn btn-primary ml-2 mt-2'></td>
        </tr>
    </form>";
    }
    echo $output;
} else {
    echo "<h2>No found</h2>";
}
$conn->close();
