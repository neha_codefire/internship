<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h2>the XMLHttpRequest</h2>
    <button type='button' onclick="loadDoc()">Request data</button>
    <p id='demo'></p>
    <script>
        function loadDoc() {
            var xhttp = new XMLHttpRequest(); //making object

            //function to call when the ready state property changes
            /*ready state:holds the status of XMLHttpRequest
            1:request not initialized
            2:server connection established
            3:processing request
            4:request finished and response is ready

            ready state 4=request finished and response is ready
            status 200=The resource has been fetched and is transmitted in the message body
            */
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById("demo").innerHTML = this.responseText;
                    //response text=returns the response data as a string
                }
            };
            //open(method,url,async,user,psw)
            /*
            specifies the request
            method: the request type GET or POST
            url: the file location
            async: true (asynchronous) or false (synchronous)
            user: optional user name
            psw: optional password
            */
            xhttp.open("GET", "neha.txt", true);
            //Sends the request to the server
            //Used for GET requests
            xhttp.send();
        }
    </script>
</body>

</html>