<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h2>Callback functions</h2>
    <p id="demo"></p>

    <script>
        function display(some) {
            document.getElementById('demo').innerHTML = some;
        }

        function getFile(callback) {
            let req = new XMLHttpRequest();
            req.open("GET", "neha.txt");
            req.onload = function() {
                if (req.status == 200) {
                    callback(this.responseText);
                } else {
                    callback("Error" + req.status);
                }
            }
            req.send();
        }
        getFile(display);
    </script>
</body>

</html>