console.log("I am learninf ajax");


let fetchbtn = document.getElementById('fetchbtn');
fetchbtn.addEventListener('click', buttonClickHandler);

function buttonClickHandler() {
    console.log('you havee clicked the fetchbtn');

    //instantiate an xhr object
    const xhr = new XMLHttpRequest();

    //open the object
    xhr.open('GET', 'https://jsonplaceholder.typicode.com/todos/1', true);

    //what to do on progress optional
    xhr.onprogress = function () {
        console.log("on progress");
    }
    // to know the state: 4 states
    xhr.onreadystatechange = function () {
        console.log('ready state is', xhr.readyState);
    }
    //what to do when response is ready
    // it means when you have to state 4
    xhr.onload = function () {
        if (this.status === 200) {
            console.log(this.responseText)
        }
        else {
            console.error("error occured");
        }

    }
    //send the request
    xhr.send();
    console.log('We afre done');

}