<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        table,
        th,
        td {
            border: 1px solid black;
            border-collapse: collapse;
        }

        th,
        td {
            padding: 5px;
        }
    </style>
</head>

<body>
    <form action="">
        <select name="customers" onchange="show(this.value)">
            <option value="">Select a customer</option>
            <option value="4">Nitin</option>
            <option value="11">Neha</option>
        </select>
    </form>
    <div id="txt">display</div>

    <script>
        function show(id) {
            var x;
            if (id == "") {
                document.getElementById('display').innerHTML = "";
                return;
            }
            x = new XMLHttpRequest();
            x.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById('txt').innerHTML = this.responseText;
                }
            };

            x.open("GET", "getUser.php?id=" + id, true);
            x.send();

        }
    </script>
</body>

</html>