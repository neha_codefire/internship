<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h2>Callback function</h2>
    <p>Do calculation and then display the result</p>

    <p id="demo"></p>

    <script>
        function display(some) {
            document.getElementById('demo').innerHTML = some;
        }

        function myCal(num1, num2, myCallback) {
            let sum = num1 + num2;
            myCallback(sum);
        }
        myCal(4, 2, display);
    </script>
</body>

</html>