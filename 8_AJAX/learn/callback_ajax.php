<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <div id="demo">
        <h2>Callback function in ajax</h2>

        <button type="button" onclick="loadDoc('neha.txt',myFunction)">Change content</button>

    </div>

    <script>
        function loadDoc(url, func) {
            var x = new XMLHttpRequest();
            x.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    func(this);
                }
            };
            x.open("GET", url, true);
            x.send();
        }

        function myFunction(x) {
            document.getElementById("demo").innerHTML = x.responseText;
        }
    </script>
</body>

</html>