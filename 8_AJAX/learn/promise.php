<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h2>js promise</h2>
    <p id="demo"></p>

    <script>
        function display(some) {
            document.getElementById("demo").innerHTML = some;
        }
        let myPromise = new Promise(function(myResolve, myReject) {
            let x = 0;

            if (x == 0) {
                myResolve('ok');
            } else {
                myReject('error');
            }
        });

        myPromise.then(
            function(value) {
                display(value);
            },
            function(error) {
                display(error);
            }
        );
    </script>
</body>

</html>