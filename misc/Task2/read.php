<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/main.css">
</head>

<body>
    <div class="container">
        <table>
            <?php
            include 'connection.php';
            $sql = "SELECT * FROM `users`";
            $result = mysqli_query($conn, $sql);
            while ($row = mysqli_fetch_assoc($result)) {
            ?>

                <!--<table>-->
                <tr>
                    <td>
                        <?php
                        echo "<b>" . $row['fname'] . " " . $row['lname'] . "<b>";
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php
                        echo "Email:&emsp;" . $row['email'];
                        ?>
                    </td>
                </tr>
                <!--</table>-->
            <?php
            }
            if (!mysqli_query($conn, $sql)) {
                echo "error" . mysqli_error($conn);
            }
            mysqli_close($conn);
            ?>
        </table>
    </div>
</body>

</html>