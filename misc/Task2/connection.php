<?php
// connection to database
$servername = "localhost";
$username = "root";
$password = "";
$database = "UserDetails";

//creating database connection

$conn = mysqli_connect($servername, $username, $password, $database);

//Check connection

if (!$conn) {
    die("Failed to connect" . mysqli_connect_error());
}
