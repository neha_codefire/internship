<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

// $this->title = 'Signup';
// $this->params['breadcrumbs'][] = $this->title;

$cssContent = '
.site-signup{
    background-color:#222222;
    color:white;
    width:50%;
    margin:auto;
    border-radius:10px;
    padding:10px;
    text-align: center;
    margin-top:40px;
    border:solid white 2px;
}
form{
    margin-left: 80%;
    text-align: center;
    width:100%;
}
';
$this->registerCss($cssContent);
// $jsContent = 'put your javascript content here';
//$this->registerJs($jsContent);
?>
<div class="site-signup">
    <h1>Sign up</h1>

    <p>Please fill out the following fields to signup:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

            <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
            <!--here mdel is passed through site controller-->

            <?= $form->field($model, 'email') ?>

            <?= $form->field($model, 'password')->passwordInput() ?>

            <div class="form-group">
                <?= Html::submitButton('Signup', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>