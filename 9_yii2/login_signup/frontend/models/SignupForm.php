<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\User;
use yii\base\ErrorException;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => Yii::$app->params['user.passwordMinLength']],
        ];
    }

    /**
     * Signs user up.
     *
     * @return bool whether the creating new account was successful and email was sent
     */
    public function signup()
    {
        $p = "";
        if (!$this->validate()) {
            $p .= "validateno";
            //return $p;
        }

        // if ($user = new User()) {
        //     $p .= " user obj";
        //     //return $p;
        // }

        // if ($user->username = $this->username) {
        //     $p .= " usernameset";
        //     //retur
        // }

        // if ($user->email = $this->email) {
        //     $p .= " emailset " . $this->password;
        //     //return $p;
        // }
        // // try {
        // //     $user->setPassword($this->password);
        // // } catch (ErrorException $e) {
        // //     $p . " error " . $e;
        // // }
        // $t = $user->setPassword($this->password);
        // $p .= " setpass" . $t;
        // //return $p;

        // if ($user->generateAuthKey()) {
        //     $p .= " genAuth";
        //     //return $p;
        // }
        // var_dump($user->save());
        // // if ($user->save()) {
        // //     $p .= " save";
        // //     //return $p;
        // // }
        // return $p;
        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        return $user->save(false);

        // $user->generateEmailVerificationToken();
        // return $user;
        // && $this->sendEmail($user);
    }

    /**
     * Sends confirmation email to user
     * @param User $user user model to with email should be send
     * @return bool whether the email was sent
     */
    protected function sendEmail($user)
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'emailVerify-html', 'text' => 'emailVerify-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setSubject('Account registration at ' . Yii::$app->name)
            ->send();
    }
}
