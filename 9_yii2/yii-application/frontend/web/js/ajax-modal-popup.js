function viewClick(className, e) {
    //for add button
    if (className == 'addButton btn btn-success') {
        $('#modal').modal('show')
            .find('#modalContent')
            .load($('.addButton').attr('value'));
    }
    //fir viewButton
    if (className == 'viewButton btn btn-success') {
        //alert("this is");
        var getData = $(e).data('model');
        $.ajax({
            url: "http://localhost/codefire/9_yii2/learn/yii-application/frontend/web/index.php?r=employee/view&id=" + getData.id,
            type: "post",
            dataType: 'json',
            success: function (data) {
                $('#modalContent').html(data);
                $('#modal').modal('show');
            },
            error: function (xhr, status, error, data) {
                alert('There was an error with your request.' + data);
            }
        })
    }
    //for editButton
    if (className == 'editButton btn btn-success') {

        var getData = $(e).data('model');
        $.ajax({
            url: "http://localhost/codefire/9_yii2/learn/yii-application/frontend/web/index.php?r=employee/update&id=" + getData.id,
            type: "post",
            dataType: 'json',
            success: function (data) {
                $('#modalContent').html(data);
                $('#modal').modal('show');
            },
            error: function (xhr, status, error) {
                alert('There was an error with your request.' + xhr.responseText);
            }
        })
    }
    if (className == 'deleteButton btn btn-success') {
        var getData = $(e).data('model');
        $.ajax({
            url: "http://localhost/codefire/9_yii2/learn/yii-application/frontend/web/index.php?r=employee/delete&id=" + getData.id,
            type: "post",
            dataType: 'json',
            success: function (data) {
                $.pjax.reload({ container: '#index' });
            },
            error: function (xhr, status, error) {
                alert('There was an error with your request.' + xhr.responseText);
            }
        })
    }
}
// function updateForm() {
//     var form = jquery('update-form');
//     var formData = new FormData($(form[0]));
//     $.ajax({
//         url: form.attr('action'),
//         type: 'POST',
//         data: formData,
//         contentType: false,
//         cache: false,
//         processData: false,
//         dataType: 'json',
//         success: function (data) {
//             alert(data);
//             if (data == 1) {
//                 //$('#modal').modal('show');
//                 //$(document).find('#modal').modal('hide');
//                 $('#modal').modal('hide');
//                 $.pjax.reload({ container: '#index' });
//             }

//         }
//     })
// }
// $('#update-form').on('beforeSubmit', function (e) {
//     e.preventDefault();
//     //var \$form=$(this);
//     var formData = new FormData($('form')[0]);
//     //console.log(formData);
//     $.ajax({
//         url: $('#update-form').attr('action'),
//         type: 'POST',
//         data: formData,
//         contentType: false,
//         cache: false,
//         processData: false,
//         dataType: 'json',
//         success: function (data) {
//             alert(data);
//             if (data == 1) {
//                 //$('#modal').modal('show');
//                 //$(document).find('#modal').modal('hide');
//                 $('#modal').modal('hide');
//                 $.pjax.reload({ container: '#index' });
//             }

//         }
//     })
// })