<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Users2 */
?>
<div class="users2-update">

    <?= $this->render('form_update', [
        'model' => $model,
    ]) ?>

</div>