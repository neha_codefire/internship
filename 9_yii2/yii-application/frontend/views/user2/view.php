<?php

use yii\widgets\DetailView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model frontend\models\Users2 */
?>
<?php Pjax::begin(['id' => 'view']); ?>
<div class="users2-view">
    <img src='assets/images/<?= $model->photo; ?>' height="200" width="200">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'first_name',
            'last_name',
            'email:email',
        ],
    ]) ?>

</div>
<?php Pjax::end(); ?>