<?php

namespace frontend\models;

use Yii;

class Employee extends \yii\db\ActiveRecord
{
    /**  
     * @inheritdoc  
     */
    public static function tableName()
    {
        return 'employee';
    }

    /**  
     * @inheritdoc  
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'email', 'pwd', 'cpwd'], 'required'],
            [['first_name', 'last_name'], 'string', 'max' => 30],
            ['email', 'email'],
            ['cpwd', 'compare', 'compareAttribute' => 'pwd'],
            [['first_name', 'last_name', 'email'], 'filter', 'filter' => 'trim', 'skipOnArray' => true],
            [['email'], 'string', 'max' => 50],
            [['pwd', 'cpwd'], 'string', 'max' => 20],
            [['photo'], 'string', 'max' => 200],
            [['email'], 'unique'],
        ];
    }
}
